package ca.guanyi.comp7031project;

import android.content.Context;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    @Mock
    Context mMockContext;


    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testAnalyzeText_isCorrect() {
        String allLetterResult = "All letters";
        String notAllLetterResult = "Not all letters";

        StringAnalyzer myObjectUnderTest = new StringAnalyzer();

        assertEquals(allLetterResult, myObjectUnderTest.analyzeText("allText"));
        assertNotEquals(allLetterResult, myObjectUnderTest.analyzeText("all7Text"));
    }


}