package ca.guanyi.comp7031project;

/**
 * Created by Guanyi on 11/25/2016.
 */

public class StringAnalyzer {
    public String analyzeText(String text) {
        char[] chars = text.toCharArray();

        for (char c : chars) {
            if(!Character.isLetter(c)) {
                return "Not all letters";
            }
        }
        return "All letters";
    }
}
